
/* German Atlantis PB(E)M Order Syntax Checker 
   Copyright (C) 1996-1998 Alexander Schroeder

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  Some of this code was taken out of the Atlantis
   1.0 source by Russel Wallace.

   Please note: This source contains information which is usually a
   secret for players of German Atlantis, namely the a complete list
   of the items and spells available in the game. Wether you choose to
   read them or not is up to you...  */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <assert.h>

/* Some Unix boxes don't have function prototypes in the header files.
   -Wall will complain about this, so here are some prototypes.  Note
   that strncasecmp () can be replaced by strnicmp () without
   restrictions.  */

int strncasecmp (const char *_s1, const char *_s2, size_t _n);

/* Definitions follow, which are used by functions copied from the
   Atlantis sources.  They are copied here in order to save ACHECK
   from having to link to practically all other Atlantis object files.  */

#define VERSION                 "6.6"

#define DISPLAYSIZE             400
#define NAMESIZE                80
#define DISPLAYED_ORDER_LENGTH  60
#define SPACE_REPLACEMENT       ("\240"[0])
#define SPACE                   ' '
#define ESCAPE_CHAR             '\\'
#define COMMENT_CHAR            ';'
#define MARGIN                  68
#define RECRUIT_COST		50

#define INDENT_FACTION		0
#define INDENT_UNIT		1
#define INDENT_ORDERS		2
#define INDENT_NEW_ORDERS	3

#define	TAXINCOME				20

#define ERR                     (use_stderr ? stderr : stdout)

long line_no;                   /* Count of line number.  */
char echo_it = 0,               /* Option: echo input lines.  */
  show_warnings = 1,            /* Option: print warnings.  */
  use_stderr = 1,               /* Option: use stderr for errors etc.  */
  brief = 0,                    /* Option: don't list errors.  */
  mem_scarce = 0;               /* Option: don't do elaborate checking.  */
int error_count = 0,            /* Counter: errors.  */
  warning_count = 0;            /* Counter: warnings.  */
char order_buf[1024];           /* Current order line.  */
char checked_buf[1024];         /* Checked order line.  */
char message_buf[1024];         /* Messages are composed here.  */
char indent;			/* Indent index.  */
FILE *F;                        /* The current file.  */
char *orders_file;              /* Points to the current filename.  */

/* ----------------- Befehle ----------------------------------- */

enum
  {
    K_ADDRESS,
    K_WORK,
    K_ATTACK,
    K_NAME,
    K_STEAL,
    K_BESIEGE,
    K_DISPLAY,
    K_ENTER,
    K_GUARD,
    K_MAIL,
    K_END,
    K_FIND,
    K_FOLLOW,
    K_RESEARCH,
    K_GIVE,
    K_ALLY,
    K_STATUS,
    K_COMBAT,
    K_BUY,
    K_CONTACT,
    K_TEACH,
    K_STUDY,
    K_DELIVER,
    K_MAKE,
    K_MOVE,
    K_PASSWORD,
    K_RECRUIT,
    K_COLLECT,
    K_SEND,
    K_QUIT,
    K_TAX,
    K_ENTERTAIN,
    K_SELL,
    K_LEAVE,
    K_CAST,
    K_RESHOW,
    K_DESTROY,
    K_COMMENT,			/* Added by Bjoern Becker , 21.07.2000 */
    K_ORIGIN,			/* Added by Bjoern Becker , 31.07.2000 */
    K_REGION,
    MAXKEYWORDS,
  };

static char *keywords[MAXKEYWORDS] =
{
  "ADRESSE",
  "ARBEITEN",
  "ATTACKIERE",
  "BENENNE",
  "BEKLAUE",
  "BELAGERE",
  "BESCHREIBE",
  "BETRETE",
  "BEWACHE",
  "BOTSCHAFT",
  "ENDE",
  "FINDE",
  "FOLGE",
  "FORSCHEN",
  "GIB",
  "HELFE",
  "KAEMPFE",
  "KAMPFZAUBER",
  "KAUFE",
  "KONTAKTIERE",
  "LEHRE",
  "LERNE",
  "LIEFERE",
  "MACHE",
  "NACH",
  "PASSWORT",
  "REKRUTIERE",
  "SAMMEL",
  "SENDE",
  "STIRB",
  "TREIBE",
  "UNTERHALTEN",
  "VERKAUFE",
  "VERLASSE",
  "ZAUBERE",
  "ZEIGE",
  "ZERSTOEREN",
  "//",					/* Added by Bjoern Becker , 21.07.2000 */
  "URSPRUNG",				/* Added by Bjoern Becker , 31.07.2000 */
  "REGION",
};

/* ----------------- Parameter --------------------------------- */

enum
  {
    P_ALL,
    P_PEASANT,
    P_LOOT,
    P_BUILDING,
    P_UNIT,
    P_BEHIND,
    P_CONTROL,
    P_NOT,
    P_NEXT,
    P_FACTION,
    P_PERSON,
    P_REGION,
    P_SHIP,
    P_SILVER,
    P_ROAD,
    P_TEMP,
    P_AND,
    P_SPELLBOOK,
    MAXPARAMS,
  };

static char *parameters[MAXPARAMS] =
{
  "ALLES",
  "BAUERN",
  "BEUTE",
  "BURG",
  "EINHEIT",
  "HINTEN",
  "KOMMANDO",
  "NICHT",
  "NAECHSTER",
  "PARTEI",
  "PERSONEN",
  "REGION",
  "SCHIFF",
  "SILBER",
  "STRASSE",
  "TEMP",
  "UND",
  "ZAUBERBUCH",
};

/* --------------- Reports Typen ------------------------------- */

enum
  {
    O_REPORT,
    O_COMPUTER,
    O_ZINE,
    O_COMMENTS,
    O_STATISTICS,
    O_ZIP,
    O_RAR,
    O_BZ2,
    O_TGZ,
    O_TXT,
    O_WMISC,
    O_WINCOME,
    O_WCOMMERCE,
    O_WPRODUCTION,
    O_WMOVEMENT,
    MAXOPTIONS,
  };

static char *options[MAXOPTIONS] =
{
  "AUSWERTUNG",
  "COMPUTER",
  "ZEITUNG",
  "KOMMENTAR",
  "STATISTIK",
  "ZIP",
  "RAR",
  "BZ2",
  "TGZ",
  "TXT",
  "VERSCHIEDENES",
  "EINKOMMEN",
  "HANDEL",
  "PRODUKTION",
  "BEWEGUNGEN",
};

/* ------------------ Talente ---------------------------------- */

enum
  {
    SK_CROSSBOW,
    SK_LONGBOW,
    SK_CATAPULT,
    SK_SWORD,
    SK_SPEAR,
    SK_RIDING,
    SK_TACTICS,
    SK_MINING,
    SK_BUILDING,
    SK_TRADE,
    SK_LUMBERJACK,
    SK_MAGIC,
    SK_HORSE_TRAINING,
    SK_ARMORER,
    SK_SHIPBUILDING,
    SK_SAILING,
    SK_QUARRYING,
    SK_ROAD_BUILDING,
    SK_STEALTH,
    SK_ENTERTAINMENT,
    SK_WEAPONSMITH,
    SK_CARTMAKER,
    SK_OBSERVATION,
    SK_TAXING,			/* Added by Bjoern Becker , 15.08.2000 */
    SK_FLETCHER,		/* Added by Bjoern Becker , 15.08.2000 */
    MAXSKILLS,
  };

static char *skillnames[MAXSKILLS] =
{
  "Armbrustschiessen",
  "Bogenschiessen",
  "Katapultbedienung",
  "Schwertkampf",
  "Speerkampf",
  "Reiten",
  "Taktik",
  "Bergbau",
  "Burgenbau",
  "Handeln",
  "Holzfaellen",
  "Magie",
  "Pferdedressur",
  "Ruestungsbau",
  "Schiffsbau",
  "Segeln",
  "Steinbau",
  "Strassenbau",
  "Tarnung",
  "Unterhaltung",
  "Waffenbau",
  "Wagenbau",
  "Wahrnehmung",
  "Steuereintreiben",		/* Added by Bjoern Becker , 15.08.2000 */
  "Bogenbau",			/* Added by Bjoern Becker , 15.08.2000 */
};

/* ---------- Regionen ----------------------------------------- */

/* Changed by Bjoern Becker , 26.07.2000 */

enum
  {
    D_NORTHEAST,
    D_NORTHWEST,
    D_SOUTHEAST,
    D_SOUTHWEST,
    D_EAST,
    D_WEST,
    MAXDIRECTIONS,
  };

/* End of Changes */

static char *directions[MAXDIRECTIONS] =
{
  "Nordosten",
  "Nordwesten",
  "Suedosten",
  "Suedwesten",
  "Osten",
  "Westen",
};

/* ---------- Schiffe ------------------------------------------ */

enum
  {
    SH_BOAT,
    SH_LONGBOAT,
    SH_DRAGONSHIP,
    SH_CARAVELL,
    SH_TRIREME,
    MAXSHIPS,
  };

static char *shiptypes[MAXSHIPS] =
{
  "Boot",
  "Langboot",
  "Drachenschiff",
  "Karavelle",
  "Trireme",
};

/* ------------------------------------------------------------- */

enum
  {
    I_SILVER,
    I_IRON,
    I_WOOD,
    I_STONE,
    I_HORSE,
    I_WAGON,
    I_CATAPULT,
    I_SWORD,
    I_SPEAR,
    I_CROSSBOW,
    I_LONGBOW,
    I_CHAIN_MAIL,
    I_PLATE_ARMOR,
    I_BALM,
    I_SPICES,
    I_JEWELERY,
    I_MYRRH,
    I_OIL,
    I_SILK,
    I_INCENSE,
    I_AMULET_OF_HEALING,
    I_AMULET_OF_TRUE_SEEING,
    I_CLOAK_OF_INVULNERABILITY,
    I_RING_OF_INVISIBILITY,
    I_RING_OF_POWER,
    I_RUNESWORD,
    I_SHIELDSTONE,
    I_WINGED_HELMET,
    I_DRAGON_PLATE,
    MAXITEMS
  };

#define LASTRESSOURCE   (I_HORSE +1)
#define isressource(i)  (0 <= i && i < LASTRESSOURCE)

#define LASTPRODUCT     (I_PLATE_ARMOR +1)
#define isproduct(i)    (0 <= i && i < LASTPRODUCT)

#define FIRSTLUXURY     (I_BALM)
#define LASTLUXURY      (I_INCENSE +1)
#define MAXLUXURIES     (LASTLUXURY - FIRSTLUXURY)
#define isluxury(i)     (FIRSTLUXURY <= i && i < LASTLUXURY)

/* itemnames in Einzahl und Mehrzahl! */

static char *itemnames[2][MAXITEMS] =
{
  {
    "Silber",
    "Eisenbarren",
    "Holz",
    "Steinquader",
    "Pferd",
    "Wagen",
    "Katapult",
    "Schwert",
    "Speer",
    "Armbrust",
    "Bogen",
    "Kettenhemd",
    "Plattenpanzer",
    "Balsamtopf",
    "Gewuerzballen",
    "Juwel",
    "Myrrhe Saeckchen",
    "Oel Amphore",
    "Seidenballen",
    "Weihrauch Kiste",
    "Amulett der Heilung",
    "Amulett des wahren Sehens",
    "Mantel der Unverletzlichkeit",
    "Ring der Unsichtbarkeit",
    "Ring der Macht",
    "Runenschwert",
    "Schildstein",
    "Helm der Sieben Winde",
    "Drachenpanzer",
  },
  {
    "Silber",
    "Eisenbarren",
    "Holz",
    "Steinquader",
    "Pferde",
    "Wagen",
    "Katapulte",
    "Schwerter",
    "Speere",
    "Armbrueste",
    "Boegen",
    "Kettenhemde",
    "Plattenpanzer",
    "Balsamtoepfe",
    "Gewuerzballen",
    "Juwelen",
    "Myrrhe Saeckchen",
    "Oel Amphoren",
    "Seidenballen",
    "Weihrauch Kisten",
    "Amulette der Heilung",
    "Amulette des wahren Sehens",
    "Maentel der Unverletzlichkeit",
    "Ringe der Unsichtbarkeit",
    "Ringe der Macht",
    "Runenschwerter",
    "Schildsteine",
    "Helme der Sieben Winde",
    "Drachenpanzer",
  }
};

/* ------- Zaubersprueche -------------------------------------- */

enum
  {
    SP_BLACK_WIND,
    SP_CAUSE_FEAR,
    SP_RUST,
    SP_DAZZLING_LIGHT,
    SP_FIREBALL,
    SP_HAND_OF_DEATH,
    SP_HEAL,
    SP_INSPIRE_COURAGE,
    SP_LIGHTNING_BOLT,
    SP_GOLEM_SERVICE,
    SP_CLAWS_OF_THE_DEEP,
    SP_MAKE_AMULET_OF_HEALING,
    SP_MAKE_AMULET_OF_TRUE_SEEING,
    SP_MAKE_CLOAK_OF_INVULNERABILITY,
    SP_MAKE_RING_OF_INVISIBILITY,
    SP_MAKE_RING_OF_POWER,
    SP_MAKE_RUNESWORD,
    SP_MAKE_SHIELDSTONE,
    SP_REMOTE_MAP,
    SP_PLAGUE,
    SP_DREAM_MAP,
    SP_SHIELD,
    SP_SUNFIRE,
    SP_TELEPORT,
    SP_MAP,
    SP_INFERNO,
    SP_HOLY_WOOD,
    SP_TREMMOR,
    SP_SUMMON_UNDEAD,
    SP_CONJURE_KNIGHTS,
    SP_STORM_WINDS,
    SP_FOG_WEBS,
    SP_NIGHT_EYES,
    SP_WATER_WALKING,
    SP_MAKE_WINGED_HELMET,
    SP_HAMMER,
    SP_PROVOCATION,
    SP_BLESSING,
    MAXSPELLS,
  };

char *spellnames[MAXSPELLS] =
{
  "Schwarzer Wind",
  "Panik",
  "Rosthauch",
  "Magische Leuchtfeuer",
  "Feuerball",
  "Hand des Todes",
  "Heilung",
  "Heldenmut",
  "Blitzschlag",
  "Dienst der Golems",
  "Klauen der Tiefe",
  "Erschaffe ein Amulett der Heilung",
  "Erschaffe ein Amulett des Wahren Sehens",
  "Erschaffe einen Mantel der Unverletzlichkeit",
  "Erschaffe einen Ring der Unsichtbarkeit",
  "Erschaffe einen Ring der Macht",
  "Erschaffe ein Runenschwert",
  "Erschaffe einen Schildstein",
  "Geisterauge",
  "Pesthauch",
  "Traumadler",
  "Schild",
  "Sonnenfeuer",
  "Teleportation",
  "Adlerauge",
  "Inferno",
  "Hain der 1000 jaehrigen Eichen",
  "Donnerbeben",
  "Totenruf",
  "Schattenritter",
  "Sturmwind",
  "Nebelnetze",
  "Auge der Nacht",
  "Wasserwandeln",
  "Erschaffe einen Helm der Sieben Winde",
  "Hammer der Goetter",
  "Provokation der Titanen",
  "Segen der Goettin",
};

char iscombatspell[MAXSPELLS] =
{
  1,
  1,
  0,
  1,
  1,
  1,
  0,
  1,
  1,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  1,
  1,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
};

char base36[36] =
{
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'L',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
};

char* tobase36 ( int dezimal)
{
  static char buffer[7];
  int first = 1;
  int c;
  int v = 60466176;


  c=0;

  while (v > 0)
  {
    if ( (dezimal >=v) || (!first) )
    {
      buffer[c] = base36[dezimal/v];
      dezimal = dezimal % v;
      c++;
      first = 0;
    };
    buffer[c+1]=0;
    v=v/36;
  };

  return buffer;
};

int todec ( char* b36)
{
  int n,n2;
  int v = 1;
  int v2 = 0;

  for (n=strlen(b36)-1; n >= 0 ; n--)
  {
    for (n2=0; n2 < 36 ; n2++)
    {
      if (tolower(b36[n]) == tolower(base36[n2]) )
      {
        v2 = v2 + (n2*v);
        v = v * 36;
      };
    };
  };

  return v2;
};


/* - Errors and Warnings --------------------------------------- */

void
error (char *s, long int l, char *o)
{
  error_count++;
  if (!brief)
    fprintf (ERR, "%s:%ld: Fehler: %s.\n"
             "  `%s'\n", orders_file, l, s, o);
}

void
warning (char *s, long int l, char *o)
{
  warning_count++;
  if (show_warnings && !brief)
    fprintf (ERR,"%s:%ld: Warnung: %s.\n"
             "  `%s'\n", orders_file, l, s, o);
	     /* "Warnung zur Zeile %ld: %s.\n"
		"  `%s'\n", l, s, o); */
}

void
warning2 (char *s, long int l, char *o)
{
  if (show_warnings > 1)
    {
      warning_count++;
      if (!brief)
	fprintf (ERR,"%s:%ld: Warnung: %s.\n"
		 "  `%s'\n", orders_file, l, s, o);
    }
}

void
limit_string (char *s)
{
  int i;
  if (strlen (s) > DISPLAYED_ORDER_LENGTH)
    {
      for (i=0; i!=3; i++)
	s[DISPLAYED_ORDER_LENGTH + i] = '.';
      s[DISPLAYED_ORDER_LENGTH + i] = 0;
    }
}

void
anerror (char *s)
{
  limit_string (order_buf);
  error (s, line_no, order_buf);
}

void
awarning (char *s)
{
  limit_string (order_buf);
  warning (s, line_no, order_buf);
}

void
amessage (char *s)
{
  if (!brief)
    fprintf (ERR, "%s.\n", s);
}


/* - Einheiten ------------------------------------------------- */

/* Diese Strukturen und Funktionen sollen wie folgt funktionieren:
   Alle (normale und TEMP) Einheiten die Befehle geben, werden
   abgespeichert.  Alle Referenzen auf temporaere Einheiten werden
   abgespeichert.

   Ein Fehler wird erzeugt, wenn dieselbe Einheit ein weiteres Mal
   Befehle gibt.

   Ein Fehler wird erzeugt, wenn mehr als ein langer Befehl gegeben
   wird, und eine Warnung wird erzeugt, wenn kein langer Befehl
   gegeben wird.  Eine Warnung wird erzeugt, wenn mehr als ein LIEFERE
   Befehl gegeben wird.

   Ein Fehler wird erzeugt, wenn eine TEMP Einheit andere TEMP
   Einheiten referenziert (z. B. mittels GIB), und eine Warnung wird
   erzeugt, wenn eine TEMP Einheit eine normale Einheit referenziert.
   Der einzige halbwegs vernuenftige Grund dafuer waere, eine TEMP
   Einheit nach Dem Rekrutieren sofort an eine andere Partei zu
   uebergeben (?).  Alle Befehle die TEMP Einheiten Silber oder
   Personen geben, notieren dies hier, damit am Ende kontrolliert
   werden kann, ob alles stimmt.  Ein Fehler wird erzeugt, wenn TEMP
   Einheiten referenziert aber nicht gemacht werden.

   Nichts soll passieren, wenn eine normale Einheit eine normale
   Einheit anspricht (es koennte ja eine Einheit einer fremden Partei
   sein).

   Ist die Option mem_scarce aktiv, soll kein cmalloc () mehr
   ausgefuehrt werden, und diese Tests werden deaktiviert.

*/

typedef struct unit
{
  struct unit *next;
  int no;
  char temp;
  int people;
  int money;
  long int start_of_orders_line;
  long int first_reference_line;
  long int long_order_line;
  long int deliver_line;
  char *start_of_orders;
  char *first_reference;
  char *long_order;
  char *deliver;
} unit;

/* Zeigt auf den Start der globalen Liste aller Einheiten.  */
unit *units;

/* Zeigt auf die Befehle gebende Einheit.  */
unit *order_unit;

/* Zeigt auf die referenzierte TEMP Einheit.  */
unit *referenced_unit;

/* Merkt sich die Befehle-gebende Einheit, wenn die Befehle einer TEMP
   Einheit bearbeitet werden.  */
unit *mother_unit;

void *
cmalloc (int n)
{
  void *p;

  if (n == 0)
    n = 1;

  p = malloc (n);

  if (p == 0)
    {
      puts ("Kein freier Speicher mehr.");
      exit (1);
    }

  return p;
}

#define addlist2(l,p)       (p->next = *l, *l = p)

void
free_units ()
{
  unit *u, *next_unit;
  
  u = units;
  while (u)
    {
      next_unit = u->next;
      if (u->start_of_orders)
	free (u->start_of_orders);
      if (u->long_order)
	free (u->long_order);
      if (u->first_reference)
	free (u->first_reference);
      free (u);
      u = next_unit;
    }
  units = 0;
}

unit *
find_unit (int i, char t)
{
  unit *u;

  for (u = units; u; u = u->next)
    if (u->no == i && u->temp == t)
      break;
  return u;
}

int
end_unit_orders (void)
{
  if (!order_unit) /* Fuer die ersten Befehle der ersten Einheit.  */
    return 0;
  if (!order_unit->long_order_line)
    {
      if (order_unit->temp)
	sprintf (message_buf, "%s %d hat keinen langen Befehl bekommen", 
		 parameters[P_TEMP], order_unit->no );
      else
	sprintf (message_buf, "Einheit %s hat keinen langen Befehl bekommen",
		 tobase36(order_unit->no) );
      /* Warnung nur falls -w2 eingestellt wurde.  */
      warning2 (message_buf, order_unit->start_of_orders_line,
	       order_unit->start_of_orders);
    }
  if (order_unit->temp)
    {
      order_unit = mother_unit;
      mother_unit = 0;
      return 1;
    }
  return 0;
}

unit *
new_unit (int i, char t)
{
  unit *u, **up=&units;

  if (mem_scarce)
    return 0;

  u = cmalloc (sizeof (unit));
  memset (u, 0, sizeof (unit));
  u->no = i;
  u->temp = t;
  addlist2 (up, u);
  return u;
}

void
new_unit_orders (unit *u)
{
  if (mem_scarce)
    return;

  assert (u && !u->start_of_orders_line);

  u->start_of_orders = cmalloc (sizeof (order_buf));
  strcpy (u->start_of_orders, order_buf);
  u->start_of_orders_line = line_no;
}

void
orders_for_unit (int i)
{
  unit *u;

  if (mem_scarce)
    return;

  end_unit_orders ();
  u = find_unit (i, 0);
  if (u && u->start_of_orders_line)
    {
      sprintf (message_buf, "Einheit %s hat schon auf Zeile %ld "
	       "Befehle bekommen", tobase36(u->no), u->start_of_orders_line);
      anerror (message_buf);
      return;
    }
  if (!u)
    u = new_unit (i, 0);
  new_unit_orders (u);
  order_unit = u;
  assert (order_unit);
}

void
orders_for_temp_unit (int i)
{
  unit *u;

  if (mem_scarce)
    return;

  u = find_unit (i, 1);
  if (u && u->start_of_orders_line)
    {
      sprintf (message_buf, "`%s %d' wurde auf Zeile %ld schon gebraucht", 
	       parameters[P_TEMP], u->no, u->start_of_orders_line);
      anerror (message_buf);
      return;
    }
  if (!u)
    u = new_unit (i, 1);
  new_unit_orders (u);
  mother_unit = order_unit;
  order_unit = u;
  assert (order_unit);
}

void
long_order (void)
{
  if (mem_scarce)
    return;
  assert (order_unit);

  if (order_unit->long_order_line)
    {
      if (order_unit->temp)
	sprintf (message_buf, "%s %d hat schon einen langen Befehl auf Zeile %ld (`%s')", 
		 parameters[P_TEMP], order_unit->no, order_unit->long_order_line, order_unit->long_order);
      else
	sprintf (message_buf, "Einheit %s hat schon einen langen Befehl auf Zeile %ld (`%s')",
		 tobase36(order_unit->no), order_unit->long_order_line, order_unit->long_order);
      anerror (message_buf);
    }
  else
    {
      order_unit->long_order = cmalloc (sizeof (order_buf));
      strcpy (order_unit->long_order, order_buf);
      order_unit->long_order_line = line_no;
    }
}

void
deliver_order (void)
{
  if (mem_scarce)
    return;
  assert (order_unit);

  if (order_unit->deliver_line)
    {
      if (order_unit->temp)
	sprintf (message_buf, "%s %d hat schon einen LIEFERE Befehl auf Zeile %ld (`%s')", 
		 parameters[P_TEMP], order_unit->no, order_unit->deliver_line, order_unit->deliver);
      else
	sprintf (message_buf, "Einheit %s hat schon einen langen Befehl auf Zeile %ld (`%s')",
		 tobase36(order_unit->no), order_unit->deliver_line, order_unit->deliver);
      anerror (message_buf);
    }
  else
    {
      order_unit->deliver = cmalloc (sizeof (order_buf));
      strcpy (order_unit->deliver, order_buf);
      order_unit->deliver_line = line_no;
    }
}

unit *
new_unit_reference (int i, char t)
{
  unit *u = new_unit (i, t);

  if (mem_scarce)
    return 0;

  assert (t && u);
  u->first_reference = cmalloc (sizeof (order_buf));
  strcpy (u->first_reference, order_buf);
  u->first_reference_line = line_no;
  return u;
}

void
reference_to_unit (int i, char t)
{
  unit *u;

  if (mem_scarce)
    return;
  assert (order_unit);

  if (order_unit->temp)
    {
      sprintf (message_buf, "%s %d ist eine %s Einheit",
	       parameters[P_TEMP], order_unit->no, parameters[P_TEMP]);
      awarning (message_buf);
    }

  if (!t)
    return;
  
  u = find_unit (i, t);
  if (!u)
    u = new_unit_reference (i, t);
  referenced_unit = u;
  assert (referenced_unit);
}

void
check_units ()
{
  unit *u;
  
  /* Alle noch bestehenden Referenzen aufloesen.  */
  referenced_unit=0;
  if (end_unit_orders ()) /* Falls innerhalb einer TEMP Einheit...  */
    {
      assert (mother_unit==0);
      end_unit_orders (); /* ...hier die richtige Einheit.  */
    }
  order_unit=0;
  
  for (u = units; u; u = u->next)
    {
      if (u->temp && u->money < 0)
        {
          sprintf (message_buf, "%s %d benoetigt noch $%d",
                   parameters[P_TEMP], u->no, -u->money);
          error (message_buf, u->first_reference_line, u->first_reference);
        }
      if (u->temp && u->people < 1)
        {
          sprintf (message_buf, "%s %d benoetigt noch Personen",
                 parameters[P_TEMP], u->no);
          if (u->first_reference_line)
	    error (message_buf, u->first_reference_line, u->first_reference);
	  else
	    error (message_buf, u->start_of_orders_line, u->start_of_orders);
        }
      if (u->temp && !u->start_of_orders_line)
	{
	  sprintf (message_buf, "%s %d wurde nicht erzeugt",
		   parameters[P_TEMP], u->no);
          error (message_buf, u->first_reference_line, u->first_reference);
	}
    }
}


/* - In/Output functions --------------------------------------- */

int
atoip (char *s)
{
  int n;

  n = atoi (s);

  if (n < 0)
    n = 0;

  return n;
}

char *
igetstr (char *s1)
{
  int i;
  static char *s;
  static char buf[512];

  if (s1)
    s = s1;
  while (s[0] == ' ')
    s++;

  i = 0;
  while (s[0] && s[0] != ' ' && i != sizeof buf)
    {
      buf[i] = s[0];

      if (s[0] == SPACE_REPLACEMENT)
        {
          if (i > 0 && buf[i - 1] == ESCAPE_CHAR)
            buf[--i] = SPACE_REPLACEMENT;
          else
            buf[i] = SPACE;
        }

      i++;
      s++;
    }

  buf[i] = 0;

  assert (i <= (int) sizeof buf);
  return buf;
}

char *
getstr (void)
{
  return igetstr (0);
}

int
geti (void)
{
  return atoip (getstr ());
}

/* Base36 Funktionen */

int
getib36 (void)
{
  return todec (getstr ());
};

int atoipb36 (char *s)
{
  int n;

  n = todec (s);

  if (n < 0)
    n = 0;

  return n;
};


/* - Getting (from current_line_buf) or Finding (in a string) keywords ------
*/

int
findstr (char **v, char *s, unsigned int n)
{

  unsigned int i;
  int position;
  char tmpbuffer[1000];
  tmpbuffer[0] = 0;

  if (!s[0])
    return -1;
  position = 0;
  for (i = 0 ; i < strlen (s); i++)
  {
    if ( (s[i] == '�') || (s[i] == '�') || (s[i] == '�') || (s[i] == '�') || (s[i] == '�') || (s[i] == '�') || (s[i] == '�') )
    {
      if ( (s[i] == '�') || (s[i] == '�') )
      {
        tmpbuffer[position] = 'a';
        tmpbuffer[position+1] = 'e';
      };
      if ( (s[i] == '�') || (s[i] == '�') )
      {
        tmpbuffer[position] = 'o';
        tmpbuffer[position+1] = 'e';
      };
      if ( (s[i] == '�') || (s[i] == '�') )
      {
        tmpbuffer[position] = 'u';
        tmpbuffer[position+1] = 'e';
      };
      if (s[i] == '�')
      {
        tmpbuffer[position] = 's';
        tmpbuffer[position+1] = 's';
      };

      position++;
      position++;
    }
    else
    {
      tmpbuffer[position]=s[i];
      position++;
    }
  };
  tmpbuffer[position] = 0;
  for (i = 0; i != n; i++)
  {
    if (v == parameters)
    {
      if (strncasecmp (v[i], tmpbuffer, strlen (v[i])) == 0)
      {
        return i;
      };
    }
    else
    {
      if (!strncasecmp (v[i], tmpbuffer, strlen (tmpbuffer)))
      {
        return i;
      };
    };
  };
  return -1;
}

int
findparam (char *s)
{
  return findstr (parameters, s, MAXPARAMS);
}

int
getparam (void)
{
  return findparam (getstr ());
}

int
igetparam (char *s)
{
  return findparam (igetstr (s));
}

int
findspell (char *s)
{
  return findstr (spellnames, s, MAXSPELLS);
}

int
finditem (char *s)
{
  int i;

  /* TODO: To be removed.  */
  if (s[0] != 0 
      && ((!strncasecmp (s, "Langbogen", 
			strlen (s) < strlen ("Langbogen")
			? strlen (s)
			: strlen ("Langbogen")))
	  || (!strncasecmp (s, "Langboegen", 
			    strlen (s) < strlen ("Langboegen")
			    ? strlen (s)
			    : strlen ("Langboegen")))))
    {
      anerror ("'Langbogen' wurde durch 'Bogen' ersetzt.");
      return I_LONGBOW;
    }

  i = findstr (itemnames[0], s, MAXITEMS);
  if (i >= 0)
    return i;

  return findstr (itemnames[1], s, MAXITEMS);
}

int
findshiptype (char *s)
{
  return findstr (shiptypes, s, MAXSHIPS);
}

int
finddirection (char *s)
{
  return findstr (directions, s, MAXDIRECTIONS);
}

int
getdirection (void)
{
  return finddirection (getstr ());
}

int
findskill (char *s)
{
  return findstr (skillnames, s, MAXSKILLS);
}

int
getskill (void)
{
  return findskill (getstr ());
}

int
findoption (char *s)
{
  return findstr (options, s, MAXOPTIONS);
}

int
getoption (void)
{
  return findoption (getstr ());
}

int
findkeyword (char *s)
{
  return findstr (keywords, s, MAXKEYWORDS);
}

int
igetkeyword (char *s)
{
  return findkeyword (igetstr (s));
}


/* - Writing checked order lines ------------------------------- */

void
scat (char *s)
{
  strcat (checked_buf, s);
}

void
space (void)
{
  scat (" ");
}

void
icat (int n)
{
  char s[20];

  sprintf (s, " %d", n);
  scat (s);
}

void
porder (void)
{
  char i;

  if (echo_it)
    {
      for (i = 0; i != indent; i++)
        fputs ("    ", stdout);
      puts (checked_buf);
    }
  checked_buf[0] = 0;
}


/* - Getting lines from the file ------------------------------- */

int
skipline (void)
{
  int c=0;

  while (c != EOF && c != '\n')
    c = fgetc (F);
  return c;
}

void
skiprest (void)
{
  int c=0, old_c=0;

  while (c != EOF && c != '"' && !(c == '\n' && old_c == '\n'))
    {
      old_c = c;
      c = fgetc (F);
    }
}

void
get_order (void)
{
  int i=0, j=0, c=0, old_c, quote=0;

  order_buf[0]=0;
  referenced_unit=0;

  for (;;)
    {

      old_c = c;
      c = fgetc (F);

      if ((c == COMMENT_CHAR && !quote))
        {
          order_buf[i] = 0;
          c = skipline ();
        }

      if (c == EOF)
        {
          order_buf[0] = (char) EOF;
          break;
        }

      if (c == '\n')
        line_no++;

      if ((c == '\n' && !quote) ||
          (c == '\n' && quote && old_c == '\n'))
        {
          order_buf[i] = 0;
          if (order_buf[i - 1] == SPACE_REPLACEMENT)
            order_buf[i - 1] = 0;
          break;
        }

      if (c == '"')
        {
          quote = !quote;
          if (quote)
            j = 0;
          continue;
        }

      if ((isspace (c) && isspace (old_c)) ||
          (j == 0 && isspace (c)))
        continue;

      j++;

      if (isspace (c))
        order_buf[i++] = quote ? SPACE_REPLACEMENT : ' ';
      else
        order_buf[i++] = c;

      if (i == sizeof order_buf)
        {
          order_buf[i] = 0;
          if (quote)
            skiprest ();
          else
            skipline ();
          break;
        }

    }
  assert (i <= (int) sizeof (order_buf));
}


/* - Getting specific stuff ------------------------------------ */

void
getafaction (char *s)
{
  int i;

  i = todec (s);
  if (!s[0])
    anerror ("Partei fehlt");
  else
    {
      if (!i)
        awarning ("Partei 0 verwendet");
      icat (i);
    }
}

int
getmoreunits (void)
{
  char *s;
  int i, count=0, temp=0;

  for (;;)
    {
      s = getstr ();
      if (findparam (s) == P_TEMP)
        {
          space ();
          scat (parameters[P_TEMP]);
	  temp = 1;
          s = getstr ();
          i = atoi (s);
        }
      else i = todec (s);

      if (!i && !count)
        anerror ("Einheit fehlt");
      if (!i)
        break;

      reference_to_unit (i, temp);
      temp = 0;
      icat (i);
      count++;
    }
  return count;
}

enum
{
  POSSIBLE,
  NECESSARY,
};

int
findaunit (char *s, int type)
{
  int i, temp=0;

  switch (findparam (s))
    {
    case P_PEASANT:
      space ();
      scat (parameters[P_PEASANT]);
      return 2;

    case P_TEMP:
      space ();
      scat (parameters[P_TEMP]);
      temp = 1;
      s = getstr ();
      i = atoi (s);
      break;

    default:
      i = todec (s);
      break;
    }

  if (s[0] == 0)
    {
      if (type == NECESSARY)
	anerror ("Einheit fehlt");
      return 0;
    }

  /* wenn in s etwas drinnen steht, aber i == 0 ist, gilt es! */
  if (s[0] && i == 0)
    awarning ("Einheit 0 verwendet");

  reference_to_unit (i, temp);
  icat (i);
  return 1;
}

int
getaunit (int type)
{
  return findaunit (getstr (), type);
}

int
isparam (char *s, int i)
{
  if (i != findparam (s))
    return 0;

  space ();
  scat (parameters[i]);
  return 1;
}

/* - String copying, wrapping and correction ------------------------ */

char *
wrap (char *s)
{
  int i, j, k;

  /* i zaehlt die laenge des strings s ab. j zaehlt die laenge der zeile ab.
     ist der rand erreicht, wird k auf i gesetzt, und rueckwaerts eine
     leerstelle gesucht, die man mit einem '\n' ersetzen koennte. */

  for (i = 0, j = 0; s[i]; i++, j++)
    {
      /* findet man eine leerstelle, wird sie durch '\n' ersetzt */

      if (j == MARGIN)
        {
          for (k = i; !isspace (s[k]) && k > 0; k--) ;
          if (k > 0)
            {
              s[k] = '\n';
              j = i - k;

              /* #zeichen, die man schon gelesen hat nach dem \n, welches man
                 hier setzt. */
            }
        }
      else
        /* hat man das letzte mal keine leerstelle gefunden, so reagiert man
           einfach auf die naechste leerstelle, ohne nach vorwaerts zu suchen
           (das hat man ja schon einmal gemacht */

      if (j > MARGIN && isspace (s[i]))
        {
          s[i] = '\n';
          j = 0;

        }
    }
  return (s);
}

void
nstrcpy (char *to, char *from, int n)
{
  n--;

  do
    if ((*to++ = *from++) == 0)
      return;
  while (--n) ;

  *to = 0;
}

void
checkstring (char *s, int l, int type)
{

  /* Aus verschiedenen Funktionen aufgerufen. Verwendet message_buf. porder
     () wird hier nicht aufgerufen. Bei einem Fehler wird auch message_buf
     verwendet. Erst danach wird s nach message_buf kopiert. */

  if ((int) strlen (s) > l)
    {
      sprintf (message_buf, "Text zu lang (max. %d)", l);
      awarning (message_buf);
    }

  if (s[0] == 0 && type == NECESSARY)
    anerror ("Text fehlt");

  assert ((int) sizeof message_buf > l);
  nstrcpy (message_buf, s, l);

  if (s[0] || type == NECESSARY)
    {
      scat (" \"");
      if (strlen (s) + strlen (checked_buf) > MARGIN)
        {
          scat ("\n");
          scat (wrap (s));
        }
      else
        scat (s);
      scat ("\"");
    }
}


/* - email Addresses ------------------------------------------- */

char *
netaddress (char *s)
{

  /* Aus checkaddr (). s enthaelt den Parameter des ADRESSE Befehles. Wir
     lesen in order_buf. */

  char *s1, *s2;

  if (!s)
    return 0;

  /* s1 zeigt auf das @ der email Adresse */

  s1 = strchr (s, '@');
  if (!s1)
    return 0;

  /* s1 sucht nach whitespace zum Anfang hin */

  while (s1 >= s && *s1 &&
         (isalnum (*s1) ||
          *s1 == '@' ||
          *s1 == '-' ||
          *s1 == '.' ||
          *s1 == '_'))
    s1--;

  s1++;

  /* Dort zeigt s1 auf den ersten Buchstaben. Nun sucht man das Ende der
     Adresse und setzt dort eine 0, damit auch ja klar ist, dass dies das
     Ende der Befehlszeile ist. */

  s2 = s1;
  while (*s2 &&
         (isalnum (*s2) ||
          *s2 == '@' ||
          *s2 == '-' ||
          *s2 == '.' ||
          *s2 == '_'))
    s2++;

  *s2 = 0;

  /* Leider habe ich keine gute Idee, um Adressen auf ihre gueltigkeit hin zu
     pruefen. */

  if (s2 - s1 > DISPLAYSIZE)
    {
      anerror ("Adresse zu lang");
      return 0;
    }

  return s1;
}

void
checkaddr (void)
{

  /* Aufgerufen von checkanorder (). Wir lesen order_buf. */

  char *s, *addr;

  scat (keywords[K_ADDRESS]);
  s = getstr ();
  checkstring (s, DISPLAYSIZE, NECESSARY);

  addr = netaddress (s);
  if (!addr)
    awarning ("Keine Zustellung per email mehr");
  else
    {
      sprintf (message_buf, "Neue email Adresse: %s", addr);
      amessage (message_buf);
    }
}


/* - check elements of the syntax ------------------------------ */

void
checknaming (void)
{
  int i;

  scat (keywords[K_NAME]);
  i = findparam (getstr ());

  switch (i)
    {
    case -1:
      anerror ("Objekt nicht erkannt");
      break;

    case P_UNIT:
    case P_FACTION:
    case P_BUILDING:
    case P_SHIP:
    case P_REGION:
      space ();
      scat (parameters[i]);
      checkstring (getstr (), NAMESIZE, NECESSARY);
      break;

    default:
      anerror ("Objekt kann nicht umbenannt werden");
    }
}

void
checkdisplay (void)
{
  int i;

  scat (keywords[K_DISPLAY]);
  i = findparam (getstr ());

  switch (i)
    {
    case -1:
      anerror ("Objekt nicht erkannt");
      break;

    case P_REGION:
    case P_UNIT:
    case P_BUILDING:
    case P_SHIP:
      space ();
      scat (parameters[i]);
      checkstring (getstr (), DISPLAYSIZE, NECESSARY);
      break;

    default:
      anerror ("Objekt kann nicht beschrieben werden");
    }
}

void
checkenter (void)
{
  int i;

  scat (keywords[K_ENTER]);
  i = findparam (getstr ());

  switch (i)
    {
    case -1:
      anerror ("Objekt nicht erkannt");
      break;

    case P_BUILDING:
    case P_SHIP:
      space ();
      scat (parameters[i]);

      i = getib36 ();
      if (!i)
        anerror ("Nummer des Objektes fehlt");
      else
        icat (i);
      break;

    default:
      anerror ("Objekt kann nicht beschrieben werden");
    }
}

enum                            /* Reihenfolge wichtig! Combat_spell = 1!! */
{
  RITUAL,
  COMBAT_SPELL,
  ANY_SPELL,
  ANY_SPELL_NO_ERROR,
};

int
getaspell (char *s, int combat_spell)
{

  /* mit parameter char *s damit man auch alternativ dazu givecontrol pruefen
     kann! *s wird also vor dem Aufruf von getaspell () mit getstr ()
     gesetzt. Fuer Meldungen wird word_buf verwendet, damit current_line_buf
     nicht ueberschrieben wird. */

  int i, u;

  i = findspell (s);
  if (i < 0)
    {
      isparam (s, P_ALL); /* schreibt automatisch den Parameter hin, falls es "ALLES" ist. */
      if (combat_spell != ANY_SPELL_NO_ERROR)
        anerror ("Zauberspruch nicht erkannt");
      return 0;
    }

  if (combat_spell != ANY_SPELL_NO_ERROR &&
      combat_spell != ANY_SPELL &&
      iscombatspell[i] != combat_spell)
    {
      sprintf (message_buf, "\"%s\" ist %s Kampfzauber", spellnames[i],
               iscombatspell[i] ? "ein" : "kein");
      anerror (message_buf);
      return 0;
    }

  scat (" \"");
  scat (spellnames[i]);
  scat ("\"");

  if (combat_spell != ANY_SPELL_NO_ERROR)       /* checkgiving () */
    switch (i)
      {
      case SP_TELEPORT:

        u = 0;
        for (;;)
          {
            i = getaunit (POSSIBLE);
            if (i == 2)
              {
                anerror ("Bauern koennen nicht teleportiert werden");
                break;
              }
            if (!i)
              break;
            u++;
          }
        if (u == 0)
          anerror ("Ziel-Einheit und eine weitere Einheit fehlen");

        if (u == 1)
          anerror ("Weitere Einheit fehlt");

        break;

      case SP_TREMMOR:
        i = getib36 ();
        if (!i)
          anerror ("Burg fehlt");
        else icat (i);
        break;

      case SP_SUMMON_UNDEAD:
      case SP_CONJURE_KNIGHTS:
        checkstring (getstr (), NAMESIZE, POSSIBLE);
        checkstring (getstr (), DISPLAYSIZE, POSSIBLE);
        break;
      }

  return 1;
}

void
checkgiving (int type)
{
  char *s;
  int i, n;

  /* GIB oder LIEFERE an wen?  */
  s = getstr ();
  switch (type)
    {
    case K_GIVE:
      findaunit (s, NECESSARY);
      break;
    case K_DELIVER:
      if (findparam (s) == P_NOT)
	{
	  space ();
	  scat (parameters[P_NOT]);
	  return;
	}
      else
	findaunit (s, NECESSARY);
      break;
    default:
      anerror ("Internal ACHECK error");
    }
  
  /* GIB oder LIEFERE was?  */
  s = getstr ();
  if (isparam (s, P_SPELLBOOK))
      return;
  if (isparam (s, P_ALL))
    {
      long_order ();
      if (referenced_unit)
	referenced_unit->people += 1;
      return;
    }
  if (!getaspell (s, ANY_SPELL_NO_ERROR)
      && !isparam (s, P_CONTROL)
      && !isparam (s, P_UNIT))
    {
      n = atoi (s);
      if (n < 1)
        anerror ("Kein Effekt");
      icat (n);

      s = getstr ();
      i = findparam (s);
      switch (i)
        {
        case P_PERSON:
          space ();
          scat (parameters[i]);
          if (referenced_unit)
	    referenced_unit->people += n;
          break;

        case P_SILVER:
          space ();
          scat (parameters[i]);
          if (referenced_unit)
	    referenced_unit->money += n;
	  break;

        default:
          i = finditem (s);
          if (i < 0)
            anerror ("Objekt nicht erkannt");
          else
            {
              scat (" \"");
              scat (itemnames[n > 1][i]);
              scat ("\"");
            }
          break;
        }
    }
}

void
getluxuries (char *s)
{
  int n, i;

  while (s[0])
    {
      i = findparam (s);
      if (i == P_AND)
	{
	  scat (parameters[i]);
	  s = getstr ();
	}
      i = findkeyword (s);
      if (i == K_BUY || i == K_SELL)
	{
	  scat (keywords[i]);
	  s = getstr ();
	}
      n = atoip (s);
      if (n > 0)
	{
	  icat (atoip (s));
	  s = getstr ();
	}
      i = finditem (s);
      if (!isluxury (i))
	{
	  anerror ("Kein Luxusgut");
	  s[0] = 0;
	}
      else
	{
	  scat (" \"");
	  scat (itemnames[n > 1][i]);
	  scat ("\"");
	}
      s = getstr ();
    }
}

void
checkmake (void)
{
  int i, m, j=0;
  char *s;

  scat (keywords[K_MAKE]);
  s = getstr ();

  i = findparam (s);
  switch (i)
    {
    case P_TEMP:
      space ();
      scat (parameters[i]);
      j = geti ();
      if (!j)
        awarning ("Keine Einheitsnummer");
      else
        {
          icat (j);
	  indent = INDENT_NEW_ORDERS;
	  orders_for_temp_unit (j);
        }
      return;

    case P_BUILDING:
    case P_SHIP:
      j = getib36 ();
    case P_ROAD:
      space ();
      scat (parameters[i]);
      if (j)
        icat (j);
      long_order ();
      return;
    }

  i = findshiptype (s);
  if (i != -1)
    {
      scat (" \"");
      scat (shiptypes[i]);
      scat ("\"");
      long_order ();
      return;
    }

  i = finditem (s);
  if (isproduct (i))
    {
      scat (" \"");
      scat (itemnames[1][i]);
      scat ("\"");
      long_order ();
      return;
    }
  /* Falls die Anzahl mit angegeben wurde.  */
  m = atoip (s);
  if (m)
    {
      icat (m);
      i = finditem (getstr ());
      if (isproduct (i))
	{
	  scat (" \"");
	  scat (itemnames[1][i]);
	  scat ("\"");
	  long_order ();
	  return;
	}
    }


  /* Zuletzt: Mann kann MACHE ohne Parameter verwenden, wenn man in
     einer Burg oder einem Schiff ist. Ist aber trotzdem eine Warnung
     wert. */
  if (s[0])                  
    anerror ("Nicht machbar");
  else
    {
      awarning ("Einheit muss in einer Burg oder einem Schiff sein");
      long_order ();
    }
}

void
checkdirections (void)
{
  int i, count;

  count = 0;
  scat (keywords[K_MOVE]);

  while ((i = getdirection ()) != -1)
    {
      space ();
      scat (directions[i]);
      count++;
    }

  if (!count && i == -1)
    anerror ("Richtung nicht erkannt");

}

void
number_list (char **s)
{
  int n;

  for (;;)
    {
      *s = getstr ();
      n = atoip (*s);
      if (!n)
        break;
      else
        icat (n);
    }
}

void
checkmail (void)
{
  char *s;

  scat (keywords[K_MAIL]);
  scat (" AN ");

  s = getstr ();
  if (findparam (s) == -1)      /* versuchs nochmal falls "an" verwendet */
    s = getstr ();

  switch (findparam (s))
    {
    case P_UNIT:
      scat (parameters[P_UNIT]);
      number_list (&s);
      break;

    case P_FACTION:
      scat (parameters[P_REGION]);
      number_list (&s);
      break;

    case P_REGION:
      scat (parameters[P_REGION]);
      s = getstr ();
      break;

    default:
      anerror ("Empfaenger fehlt");
      return;
    }

  checkstring (s, DISPLAYSIZE, NECESSARY);
}


/* - Main order checking function ------------------------------ */

void
checkanorder (void)
{

  /* Aufgerufen von readaunit (). Wir lesen order_buf. Deswegen muss hier mit
     igetkeyword () wieder von vorne begonnen werden. */

  int i;
  char *s;

  switch (igetkeyword (order_buf))
    {
    case -1:
      anerror ("Befehl nicht erkannt");
      return;

    case K_ADDRESS:
      checkaddr ();
      break;

    case K_MAIL:
      checkmail ();
      break;

    case K_WORK:
      scat (keywords[K_WORK]);
      long_order ();
      break;

    case K_ATTACK:
      scat (keywords[K_ATTACK]);
      getaunit (NECESSARY);
      break;

    case K_BESIEGE:
      scat (keywords[K_BESIEGE]);
      i = getib36 ();
      if (!i)
        anerror ("Nummer der Burg fehlt");
      else
	{
	  icat (i);
	  long_order ();
	}
      break;

    case K_NAME:
      checknaming ();
      break;

    case K_STEAL:
      scat (keywords[K_STEAL]);
      getaunit (NECESSARY);
      long_order ();
      break;

    case K_DISPLAY:
      checkdisplay ();
      break;

    case K_ENTER:
      checkenter ();
      break;

    case K_GUARD:
      scat (keywords[K_GUARD]);
      s = getstr ();
      if (findparam (s) == P_NOT)
        {
          space ();
          scat (parameters[P_NOT]);
        }
      break;

    case K_END:
      scat (keywords[K_END]);
      indent = INDENT_ORDERS;
      end_unit_orders ();
      break;

    case K_FIND:
      scat (keywords[K_FIND]);
      s = getstr ();
      if (findparam (s) == P_ALL)
        {
          space ();
          scat (parameters[P_ALL]);
        }
      else
        getafaction (s);
      break;

    case K_RESEARCH:
      scat (keywords[K_RESEARCH]);
      i = geti ();
      if (i)
        icat (i);
      long_order ();
      break;

    case K_DELIVER:
      deliver_order ();
      scat (keywords[K_DELIVER]);
      checkgiving (K_DELIVER);
      break;

    case K_GIVE:
      scat (keywords[K_GIVE]);
      checkgiving (K_GIVE);
      break;

    case K_COLLECT:
      scat (keywords[K_COLLECT]);
      s = getstr ();
      switch (i = findparam (s))
	{
	case P_ALL:
	case P_LOOT:
          space ();
          scat (parameters[i]);
	  break;
	default:
	  if ((i = atoi (s)) > 0)
	    {
	      icat (i);
	      space ();
	      scat (parameters[P_SILVER]);
	    }
	  else
	    anerror ("Silber-Betrag fehlt");
	}
      break;

    case K_ALLY:
      scat (keywords[K_ALLY]);
      getafaction (getstr ());
      s = getstr ();
      if (findparam (s) == P_NOT)
        {
          space ();
          scat (parameters[P_NOT]);
        }
      break;

    case K_STATUS:
      scat (keywords[K_STATUS]);
      s = getstr ();
      switch (findparam (s))
        {
        case P_NOT:
          space ();
          scat (parameters[P_NOT]);
          break;

        case P_BEHIND:
          space ();
          scat (parameters[P_BEHIND]);
          break;
        }
      break;

    case K_COMBAT:
      scat (keywords[K_COMBAT]);
      s = getstr ();
      getaspell (s, COMBAT_SPELL);
      break;

    case K_BUY:
      getluxuries (keywords[K_BUY]);
      long_order ();
      break;

    case K_CONTACT:
      scat (keywords[K_CONTACT]);
      getaunit (NECESSARY);
      break;

    case K_TEACH:
      scat (keywords[K_TEACH]);
      getmoreunits ();
      long_order ();
      break;

    case K_STUDY:
      scat (keywords[K_STUDY]);
      i = getskill ();
      if (i == -1)
        anerror ("Talent nicht erkannt");
      else
        {
          scat (" \"");
          scat (skillnames[i]);
          scat ("\"");
	  long_order ();
        }
      break;

    case K_MAKE:
      checkmake ();
      break;

    case K_MOVE:
      checkdirections ();
      long_order ();
      break;

    case K_FOLLOW:
      scat (keywords[K_FOLLOW]);
      getaunit (NECESSARY);
      long_order ();
      break;

    case K_PASSWORD:
      scat (keywords[K_PASSWORD]);
      s = getstr ();
      if (!s[0])
        awarning ("Passwort geloescht");
      else
        checkstring (s, NAMESIZE, POSSIBLE);
      break;

    case K_RECRUIT:
      scat (keywords[K_RECRUIT]);
      i = geti ();
      if (i)
        {
          icat (i);
	  if (!mem_scarce)
	    {
	      assert (order_unit);
	      order_unit->money -= i * RECRUIT_COST;
	      order_unit->people += i;
	    }
        }
      else
        anerror ("Anzahl Rekruten fehlt");
      break;

    case K_QUIT:
      scat (keywords[K_QUIT]);
      scat (" \"");
      scat (getstr ());
      scat ("\"");
      break;

    case K_TAX:
      scat (keywords[K_TAX]);
      i = geti () / TAXINCOME;
      /* Steuern werden in Kontingenten von $10 eingetrieben. */
      i *= TAXINCOME;
      if (!i)
	scat (" STEUERN EIN");
      else
	{
	  icat (i);
	  scat (" SILBER AN STEUERN EIN");
	}
      long_order ();
      break;

    case K_ENTERTAIN:
      scat (keywords[K_ENTERTAIN]);
      long_order ();
      break;

    case K_SELL:
      getluxuries (keywords[K_SELL]);
      long_order ();
      break;

    case K_LEAVE:
      scat (keywords[K_LEAVE]);
      break;

    case K_SEND:
      scat (keywords[K_SEND]);
      i = getoption ();
      if (i < 0)
        {
          anerror ("Option unbekannt");
          break;
        }
      space ();
      scat (options[i]);
      i = getparam ();
      if (i == P_NOT)
        {
          space ();
          scat (parameters[i]);
        }
      break;

    case K_CAST:
      scat (keywords[K_CAST]);
      s = getstr ();
      getaspell (s, RITUAL);
      long_order ();
      break;

    case K_RESHOW:
      scat (keywords[K_RESHOW]);
      s = getstr ();
      getaspell (s, ANY_SPELL_NO_ERROR);
      break;

    case K_DESTROY:
      scat (keywords[K_DESTROY]);
      break;
    }
  porder ();
}


/* - Main flow control ----------------------------------------- */

void
readaunit (void)
{

  /* Von process_order_file () aufgerufen. Liest order_buf. */

  int i;

  i = getib36 ();
  if (!i)
    {
      anerror ("Keine Einheitsnummer");
      get_order ();
      return;
    }
  orders_for_unit (i);
  icat (i);

  indent = INDENT_UNIT;
  porder ();
  indent = INDENT_ORDERS;

  for (;;)
    {
      get_order ();

      /* Erst wenn wir sicher sind, dass kein Befehl eingegeben wurde,
         checken wir, ob nun eine neue Einheit oder ein neuer Spieler
         drankommt */

      if (igetkeyword (order_buf) < 0)
        switch (igetparam (order_buf))
          {
          case P_UNIT:
          case P_FACTION:
          case P_NEXT:
            return;
          }

      /* Check ob die Datei beendet ist. */

      if (order_buf[0] == (char) EOF)
        return;

      /* Also ist es ein Befehl. */

      if (order_buf[0])
        checkanorder ();
    }
}

int
readafaction (void)
{

  /* Von process_order_file () aufgerufen. Liest order_buf. */

  int i;
  char *s;

  i = getib36 ();
  if (i)
    {
      icat (i);
      s = getstr ();
      if (s[0] == 0)
        awarning ("Kein Passwort");
      scat (" \"");
      scat (s);
      scat ("\"");
    }
  else
    anerror ("Keine Parteinummer");

  indent = INDENT_FACTION;
  porder ();
  return i;
}

void
help (const char *s)
{
  fprintf (ERR, "Benutzung: %s [Optionen] Befehlsdatei\n"
           "-  Verwendet stdin anstelle einer input datei.\n"
           "-b unterdrueckt Warnungen und Fehler (brief)\n"
           "-e schreibt die gepruefte Datei auf stdout (echo)\n"
           "-h zeigt diese kleine Hilfe an (help)\n"
	   "-m findet weniger Fehler, spart aber Speicher (low mem)\n"
           "-q unterdrueckt den Vorspann (quiet)\n"
           "-s verwendet kein stderr fuer Warnungen, Fehler etc.\n"
           "-w oder -w0 unterdrueckt alle Warnungen (warning level 0)\n"
           "-w1 zeigt die meisten Warnungen (default, warning level 1)\n"
           "-w2 zeigt alle Warnungen (warning level 2)\n", s);
}

void
end_of_faction (int f, int next)
{
  check_units ();
  free_units ();
  if (f && !next)
    {
      sprintf (message_buf, "%s fehlt", parameters[P_NEXT]);
      awarning (message_buf);
    }
}

void
process_order_file (int *faction_count, int *unit_count)
{
  int f = 0, next = 0;
  line_no = 0;
  get_order ();

  /* Auffinden der ersten Partei, und danach abarbeiten bis zur letzten
     Partei. Fuer Meldungen ("NAECHSTER fehlt") wird word_buf verwendet, weil
     in current_line_buf die aktuelle Zeile steht. */

  while (order_buf[0] != (char) EOF)
    switch (igetparam (order_buf))
      {
      case P_REGION:
        get_order();
        break;
      case P_FACTION:
	end_of_faction (f, next);
        scat (parameters[P_FACTION]);
        f = readafaction ();
        get_order ();
        (*faction_count)++;
        next = 0;
        break;

      case P_UNIT:
        if (f)
          {
            scat (parameters[P_UNIT]);
            readaunit ();
            (*unit_count)++;
          }
        else
          get_order ();
        break;

        /* falls in readunit abgebrochen wird, steht dort entweder eine neue
           partei, eine neue einheit oder das fileende. das switch wird
           erneut durchlaufen, und die entsprechende funktion aufgerufen. man
           darf current_line_buf auf alle faelle nicht ueberschreiben! Bei
           allen anderen Eintraegen hier, muss current_line_buf erneut
           gefuellt werden, da die betreffende information in nur einer Zeile
           steht, und nun die naechste gelesen werden muss. */

      case P_NEXT:
        f = 0;
        scat (parameters[P_NEXT]);
        indent = INDENT_FACTION;
        porder ();
        next = 1;

      default:
        if (f && order_buf[0])
          awarning ("Wird von keiner Einheit ausgefuehrt");
        get_order ();
      }

  /* letzte Zeile mit EOF am Anfang, also EOF mit 0 ueberschreiben */
  order_buf[0] = 0;
  end_of_faction (f, next);
}

int
main (int argc, char *argv[])
{
  int i, faction_count = 0, unit_count = 0;
  char quiet = 0;

  /* Hier in main () werden awarning () und anerror () noch nicht verwendet,
     da noch keine Befehlszeilen gelesen wurden. Also verwendet man fprintf
     (ERR, ...). Nach Fehlern bei der Bedienung von ACHECK beenden wir den
     Lauf sofort. */

  F = stdin;

  if (argc <= 1)
    {
      help (argv[0]);
      return 0;
    }

  for (i = 1; i != argc; i++)
    if (argv[i][0] == '-' ||
        argv[i][0] == '/')
      switch (argv[i][1])
        {
        case 'b':
          brief = 1;
          break;

        case 'e':
          echo_it = 1;
          break;

        case 'w':
          show_warnings = 0;
	  if (argv[i][2] == '1')
	      show_warnings = 1;
	  else if (argv[i][2] == '2')
	      show_warnings = 2;
          break;
	  
	case 'm':
	  mem_scarce = 1;
	  break;

        case 's':
          use_stderr = 0;
          break;

        case 'q':
          quiet = 1;
          break;

        case '?':
        case 'h':
          help (argv[0]);
          return 1;

        default:
          if (argv[i][1])
            {
              fprintf (ERR, "Option '%s' unbekannt.\n", argv[i]);
              return 1;
            }
        }

  /* warranty and permissions */

  puts ("ACHECK " VERSION ", the free German Atlantis Order Files Checker.\n"
        "Copyright (C) 1996, 1997 Alexander Schroeder.\n"
        "Modified by Bjoern Becker.\n\n"
        "Compiled: " __DATE__ ", " __TIME__ "\n\n");

  if (!quiet)
    puts ("The German Atlantis Order Files Checker is distributed in the hope\n"
      	  "that it will be useful, but WITHOUT ANY WARRANTY; without even the\n"
          "implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR\n"
          "PURPOSE.\n\n"

          "This program may be freely used, modified and distributed. It may\n"
          "not be sold or used commercially without prior written permission\n"
          "from the author.\n\n");

  /* argument ohne '-' vorangestellt ist input file name! Alle input Dateien
     werden abgearbeitet mit file name globbing. */

  for (i = 1; i < argc; i++)
    if (argv[i][0] != '-' &&
        argv[i][0] != '/')
      {
        if (!(F = fopen (argv[i], "r")))
          {
            fprintf (ERR, "Kann Datei `%s' nicht lesen.\n", argv[i]);
            return 2;
          }
        else
          {
            fprintf (ERR, "Verarbeite Datei `%s'.\n", argv[i]);
	    orders_file = argv[i];
	    process_order_file (&faction_count, &unit_count);
          }
      }

  /* Falls es keine input Dateien gab, ist F immer noch auf stdin
  gesetzt */

  if (F == stdin)
    process_order_file (&faction_count, &unit_count);

  fprintf (ERR, "\nEs wurden Befehle fuer %d %s und %d %s gelesen.\n",
           faction_count, faction_count != 1 ? "Parteien" : "Partei",
           unit_count, unit_count != 1 ? "Einheiten" : "Einheit");

  if (!error_count && !warning_count && faction_count && unit_count)
    fputs ("Die Befehle scheinen in Ordnung zu sein.\n", ERR);

  if (error_count > 1)
    fprintf (ERR, "Es wurden %d Fehler entdeckt.\n", error_count);
  else if (error_count == 1)
    fputs ("Es wurde ein Fehler entdeckt.\n", ERR);

  if (warning_count > 1)
    fprintf (ERR, "Es wurden %d Warnungen entdeckt.\n",
             warning_count);
  else if (warning_count == 1)
    fputs ("Es wurde eine Warnung entdeckt.\n", ERR);

  return error_count;
}
