
# ----------------------------------------------------------- #

.PHONY: all depend clean realclean mostlyclean dist
GOALS=acheck asum

all: $(GOALS)

acheck: acheck.c
	gcc -ansi -Wall -W acheck.c -oacheck

asum: asum.c
	gcc -ansi -Wall -W asum.c -oasum

clean: 
	rm -f *.o acheck asum
	rm -rf acheck-bin

install: $(GOALS)
	ln -f $^ $(HOME)/bin

# ----------------------------------------------------------- #

# Dieser Teil kann von fast allen Usern geloescht werden.  Er ist
# nicht noetig, um die Software nur zu kompilieren.  Dieser Teil
# generiert die Archive, welche ich ueber das WWW verteile.  Das ganze
# ist so kompliziert, weil ich auch DOS / Windows binaries anbiete.

# Packete zum Verteilen verschiedener Teile der Software:
#	atlantis.tar.gz	-- source code
#	acheck.zip 	-- ACHECK DOS binary
#	achk_src.zip	-- ACHECK source code

ACHECK_DIR=$(shell pwd)
DPMI_DIR=/win/djgpp/bin
DOS_DIR=/win/atlantis

# Partition mit $(DOS_DIR) muss gemounted sein und das ganze muss von
# root gestartet werden!

dist: achk_src.zip $(DOS_DIR) acheck.zip

achk_src.zip: Makefile acheck.c acheck.txt asum.c asum.txt
	zip -k $@ $^

acheck.zip: 	$(DOS_DIR)/acheck.exe \
		$(DOS_DIR)/asum.exe \
		$(ACHECK_DIR)/acheck.txt \
		$(ACHECK_DIR)/asum.txt \
		$(DPMI_DIR)/cwsdpmi.exe \
		$(DPMI_DIR)/cwsdpmi.doc \
		$(DPMI_DIR)/cwsparam.exe \
		$(DPMI_DIR)/cwsparam.doc \
		$(DPMI_DIR)/cwsdpr0.exe
	if test ! -d acheck-bin; then mkdir acheck-bin; fi
	ln -fs $^ acheck-bin
	cd acheck-bin; zip -k ../$@ *

$(DOS_DIR)/%.exe: $(DOS_DIR)/%.c
	mcopy -t -n c:/autoexec.bat /tmp
	cp /tmp/autoexec.bat /tmp/autoexec.bat.old
	echo >> /tmp/autoexec.bat
	echo d: >> /tmp/autoexec.bat
	echo cd win\\atlantis >> /tmp/autoexec.bat
	echo echo Compiling $<... >> /tmp/autoexec.bat
	echo call make.bat >> /tmp/autoexec.bat
	echo echo Done. >> /tmp/autoexec.bat
	echo pause >> /tmp/autoexec.bat
	echo c:\\exitemu >> /tmp/autoexec.bat
	mcopy -t -o /tmp/autoexec.bat c:
	dos
	mv -f /tmp/autoexec.bat.old /tmp/autoexec.bat
	mcopy -t -o /tmp/autoexec.bat c:

# Don't delete the *.c files copied to the /win partition after
# running make.
.SECONDARY: /win/atlantis/acheck.c /win/atlantis/asum.c

$(DOS_DIR)/%.c: %.c
	cp $< $@
