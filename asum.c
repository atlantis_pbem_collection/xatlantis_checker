/* Copyright 1996 Robert Schnell.
   Copyright 1996-1998 Alexander Schroeder.

   ASUM is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define NAMELENGTH     40
#define VERSION        "1.3"

static char *keywords[] = {"Statistik", "Personen" , "Einkommen"};

struct list
{
  struct list *next;
};

struct Statistikwert
{
  struct Statistikwert *next; 
  long Gesamt;
  char Name[NAMELENGTH];
} *Statistik;

void 
addlist (void *l1,void *p1)
{
  struct list **l;
  struct list *p,*q;
  
  l = l1;
  p = p1;
  
  p->next = 0;
  if (*l)
    {
      for (q = *l; q->next; q = q->next)
        ;
      q->next = p;
    }
  else
    *l = p;
}

void 
freelist (void *p1)
{
  struct list *p,*p2;
  
  p = p1;
  
  while (p)
    {
      p2 = p->next;
      free (p);
      p = p2;
    }
}

struct Statistikwert *
scan_stat (char *name)
{
  struct Statistikwert *p;
  for (p=Statistik; p; p=p->next)
    {
      if (!strcmp(name,p->Name))
        return p;
    }
  return 0;
}

void
help (const char *s)
{
  fprintf (stderr, "Benutzung: %s datei\n"
           "Anstelle eines Dateinamens kann man auch ein `-' angegeben "
           "werden. Dann wird stdin anstelle einer Datei verwendet.\n", s);
}

int 
main (int argc, char *argv[])
{
  struct Statistikwert *Stat;
  FILE *F;
  char *lp;
  char line [80];
  long Einkommen = 0;
  char name [NAMELENGTH];
  int x;

  /* Standard disclaimer.  */
  puts ("ASUM " VERSION ", the free German Atlantis Summarizer\n"
        "Version 1.0 Copyright (C) 1996 Robert Schnell\n"
        "Version 1.1 changes Copyright (C) 1996 Alexander Schroeder.\n\n"

        "The German Atlantis Order Files Checker is distributed in the hope\n"
        "that it will be useful, but WITHOUT ANY WARRANTY; without even the\n"
        "implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR\n"
        "PURPOSE.\n\n"

        "This program may be freely used, modified and distributed. It may\n"
        "not be sold or used commercially without prior written permission\n"
        "from the author.\n\n"
        
        "Compiled: " __DATE__ ", " __TIME__ "\n\n");
  
  /* Default: Using stdin to read a file.  */
  F = stdin;

  /* Help if no or too many arguments given.  */
  if (argc != 2)
    {
      help (argv[0]);
      return 0;
    }

  /* If one argument given, open the file.  */
  if (argv[1][0] != '-')
    if (!(F = fopen (argv[1], "rt")))
      {
        fprintf (stderr, "File `%s' kann nicht gelesen werden.", argv[1]);
        return 1;
      }

  while (fgets(line, 80, F) != NULL)
    {
       
      /* Check on keyword "Einkommen" */
      if (strstr (line,keywords[2]) != 0)
        {
          /* read empty line */
          fgets(line, 80, F);
          /* read first line under Einkommen */
          fgets(line, 80, F);
          do
            {
              /* set pointer on char "$" in line */
              lp = strchr (line,'$');
              /* read value after $ */
              if (lp)
                Einkommen += atol (++lp);
              /* read next lines until empty line is found */
              fgets(line, 80, F);
            }
          while (strlen(line) > 1);
        } /* if strstr ... */
       
      /* Check on keyword "Statistik" */
      if (strstr (line,keywords[0]) != 0)
        {
          /* keyword found, now scan lines for keyword "Personen" */
          /* Note : "Personen" should always occur and everything */
          /* above it is considered to be pure Region information */
          while (fgets(line, 80, F) != NULL)
            {
               
              /* Check on keyword "Personen" */
              if (strstr (line,keywords[1]) != 0)
                {
                  do
                    {
                      /* copy name up to ":" to string name */
                      memset (name,0,NAMELENGTH);
                      x = strcspn (line,":");
                      if (x > 0)
                        {
                          strncpy (name, line, x);
                        }
                      Stat = scan_stat (name);
                      /* if new Statistikwert, then make new entry in list */
                      if (!Stat) 
                        {
                          Stat = malloc (sizeof(struct Statistikwert));
                          memset (Stat,0,sizeof(struct Statistikwert));
                          Stat->Gesamt = 0;
                          strcpy (Stat->Name, name);
                          addlist (&Statistik,Stat);
                        }
                      /* set pointer to first char after ":" */
                      lp = line + x + 1;
                      /* add value to Gesamtwert */
                      Stat->Gesamt += atol (lp);
                      /* read next lines until empty line is found */
                      fgets(line, 80, F);
                    }
                  while (strlen(line) > 1);
                  break;
                } /* if strstr ... */
            }
        }
    } /* while ...*/
  /* report all found values */
  printf ("Gesamt Statistik: \n");
  Stat = Statistik;
  do
    {
      printf ("%s: %ld\n",Stat->Name,Stat->Gesamt);
    }
  while ((Stat = Stat-> next));
  printf (" Einkommen: $%ld\n",Einkommen);
  /* now let's free up memory resources */
  freelist (Statistik);

  return 0;
}
